import "./App.css";
import { useState, useEffect } from "react";
import { App_bar } from "./components/appbar";
import Cards from "./components/cards";
import { Alunos } from "./components/alunos";
import { store } from "./redux";

const axios = require("axios");

function App() {
  const [dados, setDados] = useState();

  useEffect(() => {
    axios
      .get("https://hp-api.herokuapp.com/api/characters/students")
      .then((response) => {
        console.log(response.data);
        setDados(response.data);
      });
  }, []);

  return (
    <div id="app">
      <div id="topo">
        <App_bar />
      </div>
      <Cards />
      <Alunos alunos={dados} />
    </div>
  );
}

export default App;
