import { createStore, combineReducers } from "redux";

const INITIAL = {
  value_01: 500,
  value_02: 500,
  value_03: 500,
  value_04: 500,
};

const counter = (state = INITIAL, action) => {
  console.log("reducer", state);
  switch (action.type) {
    case "add_01":
      return {
        value_01: state.value_01 + 10,
        value_02: state.value_02,
        value_03: state.value_03,
        value_04: state.value_04,
      };
    case "add_02":
      return {
        value_01: state.value_01,
        value_02: state.value_02 + 10,
        value_03: state.value_03,
        value_04: state.value_04,
      };
    case "add_03":
      return {
        value_01: state.value_01,
        value_02: state.value_02,
        value_03: state.value_03 + 10,
        value_04: state.value_04,
      };
    case "add_04":
      return {
        value_01: state.value_01,
        value_02: state.value_02,
        value_03: state.value_03,
        value_04: state.value_04 + 10,
      };
    case "sub_01":
      return {
        value_01: state.value_01 - 10,
        value_02: state.value_02,
        value_03: state.value_03,
        value_04: state.value_04,
      };
    case "sub_02":
      return {
        value_01: state.value_01,
        value_02: state.value_02 - 10,
        value_03: state.value_03,
        value_04: state.value_04,
      };
    case "sub_03":
      return {
        value_01: state.value_01,
        value_02: state.value_02,
        value_03: state.value_03 - 10,
        value_04: state.value_04,
      };
    case "sub_04":
      return {
        value_01: state.value_01,
        value_02: state.value_02,
        value_03: state.value_03,
        value_04: state.value_04 - 10,
      };

    default:
      return state;
  }
};

const reducers = combineReducers({
  counter,
});

const store = createStore(reducers);

export { store };
