import "./cardstyle.css";
import logo_01 from "../../../imagens/svg 4cobra.png";
import logo_02 from "../../../imagens/svg 4corvo.png";
import logo_03 from "../../../imagens/svg 4leao.png";
import logo_04 from "../../../imagens/svg 4topeora.png";
import { useState } from "react";
import { useDispatch } from "react-redux";

import { store } from "../../../../redux";

export const Cards = ({ aluno, render }) => {
  const dispatch = useDispatch();
  const [hide, setHide] = useState(false);
  const [hide_dois, setHide_dois] = useState(false);

  const add = (house) => {
    switch (house) {
      case "Slytherin":
        dispatch({ type: "add_01" });
        break;
      case "Ravenclaw":
        dispatch({ type: "add_02" });
        break;
      case "Gryffindor":
        dispatch({ type: "add_03" });
        break;

      case "Hufflepuff":
        dispatch({ type: "add_04" });
        break;

      default:
        break;
    }
    console.log(store.getState().counter.value_01);
    console.log(store.getState().counter.value_02);
    console.log(store.getState().counter.value_03);
    console.log(store.getState().counter.value_04);
    setHide(!hide);
  };

  const sub = (house) => {
    switch (house) {
      case "Slytherin":
        store.dispatch({ type: "sub_01" });
        break;
      case "Ravenclaw":
        store.dispatch({ type: "sub_02" });
        break;
      case "Gryffindor":
        store.dispatch({ type: "sub_03" });
        break;

      case "Hufflepuff":
        store.dispatch({ type: "sub_04" });
        break;

      default:
        break;
    }
    console.log(store.getState().counter.value_01);
    console.log(store.getState().counter.value_02);
    console.log(store.getState().counter.value_03);
    console.log(store.getState().counter.value_04);
    setHide_dois(!hide);
  };

  return (
    <>
      <div id="fundo">
        <div id="personagem">
          <img src={aluno.image} id="imagem"></img>
          {aluno.house === "Slytherin" ? (
            <img id="logo_casa" src={logo_01} />
          ) : aluno.house === "Ravenclaw" ? (
            <img id="logo_casa" src={logo_02} />
          ) : aluno.house === "Gryffindor" ? (
            <img id="logo_casa" src={logo_03} />
          ) : (
            <img id="logo_casa" src={logo_04} />
          )}

          <div id="casas">{aluno.house}</div>
          <div id="cardName">{aluno.name}</div>
          <div id="barraAleatoria"></div>

          <div
            id="gain"
            onClick={() => {
              add(aluno.house);
            }}
          >
            Gain
          </div>
          <div
            id="lose"
            onClick={() => {
              sub(aluno.house);
            }}
          >
            Lose
          </div>
          {hide && (
            <div id="hide">
              <div id="soma">+ 10</div>
              <div id="done" onClick={() => render(false)}>
                done
              </div>
            </div>
          )}
          {hide_dois && (
            <div id="hide_dois">
              <div id="soma_dois">- 10</div>
              <div id="done" onClick={() => render(false)}>
                done
              </div>
            </div>
          )}
        </div>
      </div>
    </>
  );
};
