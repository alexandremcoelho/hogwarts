import "../../../App.css";
import card from "../../imagens/card.png";
import { store } from "../../../redux";
import { useState } from "react";
import { Cards } from "./card";
export const Aluno = ({ aluno }) => {
  const [render, setRender] = useState(false);
  return (
    <>
      <div id="linha" />
      <div className="item">
        <div id="nome">{aluno.name}</div>
        <div>{aluno.house}</div>
        {/* <button id="card" style={{ backgroundImage: card }}></button> */}
        <img
          src={card}
          id="card"
          onClick={() => {
            setRender(!render);
          }}
        />
      </div>
      {render && <Cards aluno={aluno} render={setRender} />}
    </>
  );
};
