import "../../App.css";
import { Aluno } from "./aluno";

export const Alunos = ({ alunos }) => {
  return (
    <div id="alunos">
      <div id="tituloAlunos">Alunos</div>
      <div id="nome">Nome</div>
      <div id="casa">Casa</div>

      {alunos &&
        alunos.map((item, index) => <Aluno key={index} aluno={item} />)}
    </div>
  );
};
