import "../../App.css";
import cobra from "../imagens/svg 4cobra.png";
import corvo from "../imagens/svg 4corvo.png";
import leao from "../imagens/svg 4leao.png";
import topeira from "../imagens/svg 4topeora.png";
import { store } from "../../redux";
import { useSelector } from "react-redux";

const Cards = () => {
  const casa_01 = useSelector((state) => state.counter.value_01);
  const casa_02 = useSelector((state) => state.counter.value_02);
  const casa_03 = useSelector((state) => state.counter.value_03);
  const casa_04 = useSelector((state) => state.counter.value_04);

  return (
    <div id="cards">
      <div id="cobra">
        <div id="topo_01"># slytherin</div>
        <img src={cobra} id="img1" />
        <div id="pontos_01">{casa_01}</div>
      </div>
      <div id="corvo">
        <div id="topo_02"># ravenclow</div>
        <img src={corvo} id="img2" />
        <div id="pontos_02">{casa_02}</div>
      </div>
      <div id="leao">
        <div id="topo_03"># gryffindor</div>
        <img src={leao} id="img3" />
        <div id="pontos_03">{casa_03}</div>
      </div>
      <div id="topeira">
        <div id="topo_04"># hufflepuff</div>
        <img src={topeira} id="img4" />
        <div id="pontos_04">{casa_04}</div>
      </div>
    </div>
  );
};

export default Cards;
