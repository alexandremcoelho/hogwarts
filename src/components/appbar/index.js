import logo from "../imagens/logo.png";
import "../../App.css";

export const App_bar = () => {
  return (
    <div id="appbar">
      <img src={logo} id="logo_00" />
      <div id="titulo">Hogwarts Score Manager</div>
    </div>
  );
};
